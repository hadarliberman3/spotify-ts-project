
import songModel from "../modules/song/song.model.js";
import artistModel from "../modules/artist/artist.model.js";
import playlistModel from "../modules/playlist/playlist.model.js";
import { Isong,IeditSong } from "../modules/song/song.interface.js";

export async function createSongDB(song:Isong,artistId:string){
    const newSong=await songModel.create(song);
    const artist=await artistModel.findById(artistId);
    artist.songs.push(newSong._id);
    await artist.save();
    return newSong;  
}

export async function getAllSongsDB (){
    return await songModel.find()
    .populate("artist","-_id first_name last_name")
    .populate("playlist","-_id name");
    
}

export async function getOneSongDB(songId:string){
    const song = await songModel.findById(songId)
    .populate("artist","-_id first_name last_name")
    .populate("playlist","-_id name");
    return song;
}

export async function updateSongDB(songId:string,song:IeditSong){
    return await songModel.findByIdAndUpdate(songId,song,{new: true, upsert: false });
}

export async function deleteSongDB(songId:string){

        const song = await songModel.findByIdAndRemove(songId);
        const artist=await artistModel.findById(song.artist);
    
        //delete from artist
        artist.songs=artist.songs.filter((cur:string)=>{
            console.log(cur,songId);
            return cur!=songId;
        });
        await artist.save();

        //delete from playlists
        for(const curPlaylistId of song.playlist){
            const playlist= await playlistModel.findById(curPlaylistId);
            playlist.songs=playlist.songs.filter((curSong:any)=>curSong._id.toString()!=songId);
            await playlist.save();
        }
        
        return song;
}
