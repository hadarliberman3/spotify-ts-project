import { NextFunction, Request, Response } from "express";
import fs from "fs/promises";


export function logHttpToFile(filePath:string){
return async function(req:Request,res:Response,next:NextFunction){
    const line=`${req.method}||${req.url}|| ${Date.now()}\n`;
    await fs.appendFile(filePath,line);
   next();

   };
}