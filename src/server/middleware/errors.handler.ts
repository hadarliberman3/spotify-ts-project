import log from "@ajar/marker";
const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;
import { Request, Response, NextFunction } from "express";
import  fs  from "fs/promises";
import {MyErrorException,URLnotFound} from "../exceptions/errors.js";
// import { IresponseError } from "../interfaces/user.interface.js";

// export const errorConsolePrint =  (err:Error, req:Request, res:Response, next:NextFunction) => {
//     console.log(err);
//     next(err);
// };

// export const error_handler2 =  (err:Error, req:Request, res:Response, next:NextFunction) => {
//     if(NODE_ENV !== "production")res.status(500).json({status:err.message,stack:err.stack});
//     else res.status(500).json({status:"internal server error..."});
// };

// export const urlNotFoundHandler =  (req:Request, res:Response, next:NextFunction) => {
//     console.log(`url: ${White}${req.url}${Reset}${Red} not found...`);
//     next(new URLnotFound(req.url));
// };

export function logToFileErrorMW(path:string){
    return async function(err:Error,req:Request,res:Response,next:NextFunction){
        console.log(err);
        await fs.appendFile(path,`${err}\n`);
        next(err);
    };
}


export function PrintErrorMW(error:MyErrorException, req: Request, res: Response, next: NextFunction){
    console.log(`error status: ${error.status} error message: ${error.message}`);
    next(error);
}

export function ResponseErrorMW(error:MyErrorException, req: Request, res: Response, next: NextFunction){
    log.blue(error.status);
    
    const output:any={
        status:error.status||500,
        message:error.message||"http error",
        stack:error.stack
    };
    log.magenta(output.status);
    if(NODE_ENV !== "production")res.status(output.status).json({status:output.status,message:output.message,stack:output.stack});
    else res.status(output.status).json({status:output.message});
}

export function URLnotFoundMW(req: Request, res: Response, next: NextFunction){
    next(new URLnotFound(req.url));
}


