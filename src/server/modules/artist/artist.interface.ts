export interface Iartist{
    first_name:string,
    last_name:string,
    songs:string[]
}

// export type IeditArtist=Pick<Iartist, "first_name"|"last_name"|"songs">;
export interface IeditArtist{
    first_name?:string,
    last_name?:string,
    songs?:string[]
}