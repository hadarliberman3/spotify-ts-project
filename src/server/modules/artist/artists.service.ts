
import { Iartist, IeditArtist} from "./artist.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";
import { updateArtistDB,createArtistDB,deleteArtistDB,getAllArtistsDB,getOneArtistDB } from "../../db/repository.artist.js";


export async function createArtistService(artist:Iartist){
    return await createArtistDB(artist);
}

export async function getAllArtistsService (){
    return await getAllArtistsDB();   
}

export async function getOneArtistService(artistId:string,path:string){
    const artist = await getOneArtistDB(artistId);
    if (!artist) throw new URLnotFound(path);
    return artist;
}

export async function updateArtistService(artistId:string,artist:IeditArtist,path:string){
    const updatedArtist=await updateArtistDB(artistId,artist);
    if(!updatedArtist) throw new URLnotFound(path);
    return updatedArtist;
}


export async function deleteArtistService(artistId:string, path:string){
    const artist = await getOneArtistDB(artistId);
    if (!artist) throw new URLnotFound(path);
    return await deleteArtistDB(artistId);
}