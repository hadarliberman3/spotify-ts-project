import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {createArtistService,getAllArtistsService,getOneArtistService,
    updateArtistService,deleteArtistService} from "./artists.service.js";
const router = express.Router();
router.use(express.json());


router.post("/", raw(async function createArtist(req:Request,res:Response){
    const newArtist = await createArtistService(req.body);
    res.status(200).json(newArtist);

}));
// GET ALL USERS
router.get("/", raw(async function getAllArtists(req:Request,res:Response){
    const artists = await getAllArtistsService();
    res.status(200).json(artists);

}));



//get one artist
router.get("/:id", raw(async function getOneArtist (req:Request, res:Response){
    const artist = await getOneArtistService(req.params.id,req.path);
    res.status(200).json(artist);
}));

router.put("/:id", raw(async function updateArtist(req:Request, res:Response){
    const artist = await updateArtistService(req.params.id,req.body,req.path);
    res.status(200).json(artist);
}));

router.delete("/:id", raw(async function deleteArtist(req:Request, res:Response){
    const artist = await deleteArtistService(req.params.id,req.path);
    res.status(200).json(artist);
    
}));



export default router;