import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {createPlaylistService,getAllPlaylistsService,getOnePlaylistService,
    updatePlaylistService,
    AddSongToPlaylistService,deletePlaylistService,deleteSongFromPlaylistService} from "./playlist.service.js";
const router = express.Router();
router.use(express.json());

//create playlist
router.post("/", raw(async function createPlaylist(req:Request,res:Response){
    const newPlaylist = await createPlaylistService(req.body);
    res.status(200).json(newPlaylist);

}));

//add song to playlist
router.post("/:playlistId/playlist/:songId", raw(async function createPlaylist(req:Request,res:Response){
    const updatedPlaylist = await AddSongToPlaylistService(req.params.playlistId,req.params.songId,req.path);
    res.status(200).json(updatedPlaylist);

}));
// GET ALL USERS
router.get("/", raw(async function getAllPlaylists(req:Request,res:Response){
    const playlists = await getAllPlaylistsService();
    res.status(200).json(playlists);

}));

export default router;




router.get("/:id", raw(async function getOnePlaylist (req:Request, res:Response){
    const playlist = await getOnePlaylistService(req.params.id,req.path);
    res.status(200).json(playlist);
}));

router.put("/:id", raw(async function updatePlaylist(req:Request, res:Response){
    const playlist = await updatePlaylistService(req.params.id,req.body);
    res.status(200).json(playlist);
}));

router.delete("/:id", raw(async function deletePlaylist(req:Request, res:Response){
    console.log("hereeeeee");
    const playlist = await deletePlaylistService(req.params.id,req.path);
    res.status(200).json(playlist);
    
}));

router.delete("/:playlistId/playlist/:songId", raw(async function deleteSongFromPlaylist(req:Request, res:Response){
    const playlist = await deleteSongFromPlaylistService(req.params.playlistId,req.params.songId,req.path);
    res.status(200).json(playlist);
    
}));
// export async function pagination(req:Request, res:Response){
//   const page=Number(req.params.page);
//   const count=Number(req.params.count);
//   const users = await paginationService(count,page);
//   res.status(200).json(users);
// };