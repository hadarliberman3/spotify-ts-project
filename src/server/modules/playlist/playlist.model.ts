import mongoose from "mongoose";
const { Schema , model  } = mongoose;
import {SongSchema} from "../song/song.model.js";

export const PlaylistSchema = new Schema({
    name: {type: String,required: true},
    songs: [SongSchema],
    geners:[{ type: String }],
    users: [{type: Schema.Types.ObjectId, ref:"user"}]

}, {timestamps:true});


export default model("playlist", PlaylistSchema);
