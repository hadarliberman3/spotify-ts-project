
import { Iplaylist, IeditPlaylist } from "./playlist.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";
import { createPlaylistDB, getAllPlaylistsDB,getOnePlaylistDB,
    deleteSongFromPlaylistDB,deletePlaylistDB,AddSongToPlaylistDB, updatePlaylistDB} from "../../db/repository.playlist.js";


export async function createPlaylistService(playlist:Iplaylist){
    return await createPlaylistDB(playlist);
}

export async function getAllPlaylistsService (){
    return getAllPlaylistsDB();  
};

export async function getOnePlaylistService(playlistId:string,path:string){
    const playlist = await getOnePlaylistDB(playlistId);
    if (!playlist) throw new URLnotFound(path);
    return playlist;
}

export async function updatePlaylistService(playlistId:string,playlist:IeditPlaylist){
    return await updatePlaylistDB(playlistId,playlist);
}

export async function deleteSongFromPlaylistService(playlistId:string, songId:string,path:string){
    const playlist=await deleteSongFromPlaylistDB(playlistId,songId);
    if (!playlist) throw new URLnotFound(path);
    return playlist;

}

export async function deletePlaylistService(playlistId:string, path:string){
        const playlist = await deletePlaylistDB(playlistId);
        if (!playlist) throw new URLnotFound(path);

        return playlist;

}

export async function AddSongToPlaylistService(playlistId:string, songId:string,path:string){
    const playlist = await AddSongToPlaylistDB(playlistId,songId);
    if (!playlist) throw new URLnotFound(path);
    return playlist;
}

