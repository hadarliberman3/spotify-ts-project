
import { Isong } from "../song/song.interface.js";
export interface Iplaylist{
    name:string,
    songs:Isong[],
    geners:string[]
}

export interface IeditPlaylist{
    name?:string,
    songs?:Isong[],
    geners?:string[]
}