import { Iuser } from "../user/user.interface.js";
import { deleteRefreshTokenDB } from "../../db/repository.user.js";
import jwt from "jsonwebtoken";
import ms from "ms";
import {getUser} from "../../db/repository.auth.js";
import {  URLnotFound } from "../../exceptions/errors.js";
const {CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;
import bcrypt from "bcryptjs";




export async function createTokens(user:any){
    const accessToken = jwt.sign({id : user._id}, APP_SECRET as string, {
        expiresIn: ACCESS_TOKEN_EXPIRATION 
        });
    
        const refreshToken = jwt.sign({ id : user._id , profile: JSON.stringify(user)}, APP_SECRET as string, {
            expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
        });
        return {"refreshToken":refreshToken,"accessToken":accessToken};  
}

export async function deleteRefreshTokenService(userId:string){
    await deleteRefreshTokenDB(userId);
}

export async function loginService(user:Iuser,path:string){
  
const {nickname,password} = user;
const userDB=await getUser(nickname);
console.log(userDB);
if(!userDB) throw new URLnotFound(path);
const correctPassword= await bcrypt.compare(password, userDB.password);
console.log(correctPassword);
return correctPassword?userDB:false;
}



