import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import {createSongService,getAllSongsService,getOneSongService,
    updateSongService,deleteSongService} from "./song.service.js";
const router = express.Router();
router.use(express.json());

//create song
router.post("/", raw(async function createSong(req:Request,res:Response){
    const newsong = await createSongService(req.body, req.body.artist);
    res.status(200).json(newsong);
}));
// GET ALL USERS
router.get("/", raw(async function getAllSongs(req:Request,res:Response){
    const songs = await getAllSongsService();
    res.status(200).json(songs);
}));


router.get("/:id", raw(async function getOneSong (req:Request, res:Response){
    const song = await getOneSongService(req.params.id,req.path);
    res.status(200).json(song);
}));

router.put("/:id", raw(async function updateSong(req:Request, res:Response){
    const song = await updateSongService(req.params.id,req.body,req.path);
    res.status(200).json(song);
}));

router.delete("/:id", raw(async function deleteSong(req:Request, res:Response){
    const song = await deleteSongService(req.params.id,req.path);
    res.status(200).json(song);
    
}));

// router.post("/:songId/artist/:artistId", raw(async function addArtistToSong(req:Request,res:Response){
//     const connectedSong=await addArtistToSongService(req.params.songId,req.params.artistId);
//     res.status(200).json(connectedSong);
// }));

export default router;
