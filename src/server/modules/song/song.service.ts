
import { Isong, IeditSong} from "./song.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";
import { createSongDB, getAllSongsDB,getOneSongDB,updateSongDB,deleteSongDB } from "../../db/repository.song.js";


export async function createSongService(song:Isong,artistId:string){
    return await createSongDB(song,artistId);
}

export async function getAllSongsService (){
    return await getAllSongsDB();   
}

export async function getOneSongService(songId:string,path:string){
    const song = await getOneSongDB(songId);
    if(!song) throw new URLnotFound(path);
    return song;
}

export async function updateSongService(songId:string,song:IeditSong,path:string){
    const newSong=await updateSongDB(songId,song);
    if(!newSong) throw new URLnotFound(path);
    return newSong;
}

export async function deleteSongService(songId:string, path:string){
    const song = deleteSongDB(songId);
    if (!song) throw new URLnotFound(path);
    return song;
}
