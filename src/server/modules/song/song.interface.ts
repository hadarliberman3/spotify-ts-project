export interface Isong{
    name:string,
    singer: string
}

export interface IeditSong{
    name?:string,
    singer?: string
}