export interface Iuser{
    _id?:string,
    nickname:string,
    password:string,
    playlist_list:string[],
    refresh_token?:string
}

export interface IeditUser{
    _id?:string,
    nickname?:string,
    password?:string,
    playlist_list?:string[]
}