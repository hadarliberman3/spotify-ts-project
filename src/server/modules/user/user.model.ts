import mongoose from "mongoose";
const { Schema , model  } = mongoose;
import { PlaylistSchema } from "../playlist/playlist.model.js";

export const UserSchema = new Schema({
    nickname: {type: String,required: true},
    password:{type:String,required: true},
    playlist_list: [PlaylistSchema],
    refresh_token:{type:String}
}, {timestamps:true});


export default model("user", UserSchema);
