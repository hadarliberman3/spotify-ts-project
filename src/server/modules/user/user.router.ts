import express from "express";
import { Request,Response } from "express";
import raw from "../../middleware/route.async.wrapper.js";
import { createUserService, getAllUsersService, getOneUserService,
    updateUserService,deleteUserService,addPlaylistToUserService } from "./user.service.js";

const {APP_SECRET}=process.env;

const router = express.Router();
router.use(express.json());

//create song
router.post("/", raw(async function createUser(req:Request,res:Response){
    const newUser = await createUserService(req.body);
    console.log(newUser);

    res.status(200).json(newUser);
}));

//add playlist to user
router.post("/:userId/user/:playlistId", raw(async function addPlaylistToUser(req:Request,res:Response){
    const newUser = await addPlaylistToUserService(req.params.userId, req.params.playlistId);
    res.status(200).json(newUser);
}));
// GET ALL USERS
router.get("/", raw(async function getAllUsers(req:Request,res:Response){
    const Users = await getAllUsersService();
    res.status(200).json(Users);
}));


router.get("/:id", raw(async function getOneUser (req:Request, res:Response){
    const User = await getOneUserService(req.params.id,req.path);
    res.status(200).json(User);
}));

router.put("/:id", raw(async function updateUser(req:Request, res:Response){
    const User = await updateUserService(req.params.id,req.body,req.path);
    res.status(200).json(User);
}));

router.delete("/:id", raw(async function deleteUser(req:Request, res:Response){
    const User = await deleteUserService(req.params.id,req.path);
    res.status(200).json(User);
    
}));


export default router;
