
import { Iuser, IeditUser} from "./user.interface.js";
import { URLnotFound } from "../../exceptions/errors.js";
import { createUserDB, getAllUsersDB,getOneUserDB,updateUserDB,deleteUserDB,addPlaylistToUserDB } from "../../db/repository.user.js";
import bcrypt from "bcryptjs";


export async function createUserService(user:Iuser){
    console.log(user.password);
    const hash= await bcrypt.hash(user.password, 8);
    user.password=String(hash);
    return await createUserDB(user);
    ;
   
}

export async function getAllUsersService (){
    return await getAllUsersDB();   
}

export async function getOneUserService(userId:string,path:string){
    const user = await getOneUserDB(userId);
    if(!user) throw new URLnotFound(path);
    return user;
}

export async function updateUserService(userId:string,user:IeditUser,path:string){
    const newUser=await updateUserDB(userId,user);
    if(!newUser) throw new URLnotFound(path);
    return newUser;
}

export async function deleteUserService(userId:string, path:string){
    const user = deleteUserDB(userId);
    if (!user) throw new URLnotFound(path);
    return user;
}

export async function addPlaylistToUserService(userId:string,playlistId:string){
    return await addPlaylistToUserDB(userId,playlistId);

}

